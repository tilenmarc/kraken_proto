package marketplace

import (
	"context"
	"fmt"

	"strconv"

	"bitbucket.org/tilenmarc/kraken_proto/data_consumer"
	"cloud.google.com/go/firestore"
	"firebase.google.com/go"
	"google.golang.org/api/option"
)

// MarketPlaceMPC works as a connection between data providers, data consumers and computation
// nodes. It waits for a request by a data consumer, and sends requests to computation nodes
// to compute requested function on requested data.
func MarketPlaceMPC(errChan chan error) {
	// connect to the database
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		errChan <- err
		return
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		errChan <- err
		return
	}
	defer client.Close()

	iter := client.Collection("DataRequests").Doc("MPC").Snapshots(ctx)
	if err != nil {
		errChan <- err
		return
	}
	defer iter.Stop()
	// TODO: make better request system, Apache Kafka?
	_, err = iter.Next()
	if err != nil {
		errChan <- err
		return
	}

	fmt.Println("MarketplaceMPC is running")
	// wait for the requests by data consumer and evoke
	// computation nodes upon request
	for {
		docsnap, err := iter.Next()
		if err != nil {
			fmt.Println("marktplace: bad request", err)
			continue
		}
		fmt.Println("marktplace: request recieved")

		var req consumer.Request
		err = docsnap.DataTo(&req)
		if err != nil {
			fmt.Println("marktplace: bad request", err)
			continue
		}

		evokeNotes(&req, client)
	}

	return
}

func evokeNotes(request *consumer.Request, client *firestore.Client) {
	ctx := context.Background()
	for i := 0; i < 3; i++ {
		client.Collection("DataRequests").Doc("NodeRequests").Collection("Nodes").Doc(strconv.Itoa(i)).Set(ctx, request)
	}
}
