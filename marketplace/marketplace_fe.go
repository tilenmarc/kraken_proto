package marketplace

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"math/big"

	"bitbucket.org/tilenmarc/kraken_proto/data_consumer"
	"bitbucket.org/tilenmarc/kraken_proto/data_provider"
	"cloud.google.com/go/firestore"
	"firebase.google.com/go"
	"github.com/fentec-project/gofe/data"
	"github.com/fentec-project/gofe/innerprod/fullysec"
	"google.golang.org/api/option"
)

func MarketPlaceFE(errChan chan error){
	// connect to the database
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		errChan <- err
		return
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		errChan <- err
		return
	}
	defer client.Close()

	iter := client.Collection("DataRequests").Doc("FE").Snapshots(ctx)
	if err != nil {
		errChan <- err
		return
	}
	defer iter.Stop()
	// TODO: make better
	_, err = iter.Next()
	if err != nil {
		errChan <- err
		return
	}
	fmt.Println("MarketplaceFE is running")

	// wait for the requests by data consumer and upon request
	// use FE key to compute the result and send it
	for {
		docsnap, err := iter.Next()
		if err != nil {
			fmt.Println("bad request", err)
			continue
		}
		fmt.Println("request recieved")

		var req consumer.Request
		err = docsnap.DataTo(&req)
		if err != nil {
			fmt.Println("bad request", err)
			continue
		}
		res, err := calcFE(req.UserName, req.VecName, req.FuncName, client)
		if err != nil {
			fmt.Println("bad request", err)
			sendFailFE(err, client, req)
			continue
		}

		_, err = client.Collection("Results").Doc(req.Name).Set(ctx, consumer.Result{Res: res.String()})
		if err != nil {
			fmt.Println("bad request", err)
			sendFailFE(err, client, req)
			continue
		}

		fmt.Println("result provided")
	}

	return
}

func sendFailFE(err error, client *firestore.Client, req consumer.Request) {
	// send the fail to the database
	ctx := context.Background()
	_, newErr := client.Collection("Results").Doc(req.Name).Set(ctx, map[string]string{"error": err.Error()})
	if newErr != nil {
		fmt.Println("failed sending an error")
	}
}

// calcFE is a helping function that upon a request connects to the database
// downloads the cipertext and FE key and calculates the result
func calcFE(user string, vecName string, funcName string, client *firestore.Client) (*big.Int, error) {
	docsnap, err := client.Collection("EncryptedData").Doc("FE").Collection(user).Doc(vecName).Get(context.Background())
	if err != nil {
		return nil, err
	}

	var cipherData provider.ProviderCipherFE
	err = docsnap.DataTo(&cipherData)
	if err != nil {
		return nil, err
	}

	// extract cipher
	var cipher data.Vector
	bufCipher := new(bytes.Buffer)
	bufCipher.Write(cipherData.Cipher)

	decoderCipher := gob.NewDecoder(bufCipher)
	err = decoderCipher.Decode(&cipher)
	if err != nil {
		return nil, err
	}

	docsnap, err = client.Collection("Functions").Doc("FE").Collection(user).Doc(funcName).Get(context.Background())
	if err != nil {
		return nil, err
	}

	var keyData provider.ProviderDerivedKeyFE
	err = docsnap.DataTo(&keyData)
	if err != nil {
		return nil, err
	}

	// extract FEkey
	var feKey fullysec.DamgardDerivedKey
	bufKey := new(bytes.Buffer)
	bufKey.Write(keyData.FEKey)

	decoderKey := gob.NewDecoder(bufKey)
	err = decoderKey.Decode(&feKey)
	if err != nil {
		return nil, err
	}
	// extract inner product vector
	var y data.Vector
	bufY := new(bytes.Buffer)
	bufY.Write(keyData.InnerVec)

	decoderInner := gob.NewDecoder(bufY)
	err = decoderInner.Decode(&y)
	if err != nil {
		return nil, err
	}

	// calculate the result
	bound := big.NewInt(1000)
	scheme, err := fullysec.NewDamgardPrecomp(len(cipher)-2, 2048, bound)

	res, err := scheme.Decrypt(cipher, &feKey, y)

	return res, err
}
