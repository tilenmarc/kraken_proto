package marketplace

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"os/exec"
	"strconv"

	"bitbucket.org/tilenmarc/kraken_proto/data_consumer"
	"bitbucket.org/tilenmarc/kraken_proto/data_provider"
	"cloud.google.com/go/firestore"
	"firebase.google.com/go"
	"github.com/fentec-project/gofe/data"
	"google.golang.org/api/option"
	"time"
)

var scaleMambaPath = "/SCALE-MAMBA/"

// NodeMPC represents a computation node in MPC. It is run on a chosen server where it waits for a
// request by a data consumer, and on obtained request runs a SCALE-MAMBA to compute the requested
// value.
func NodeMPC(nodeId int, errChan chan error) {
	// connect to the database
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		errChan <- err
		return
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		errChan <- err
		return
	}
	defer client.Close()

	// create an instance through which requests will be received
	iter := client.Collection("DataRequests").Doc("NodeRequests").Collection("Nodes").Doc(strconv.Itoa(nodeId)).Snapshots(ctx)
	if err != nil {
		errChan <- err
		return
	}
	defer iter.Stop()
	_, err = iter.Next()
	if err != nil {
		errChan <- err
		return
	}

	fmt.Println("node " + strconv.Itoa(nodeId) + " is running")

	scaleStatusChan := make(chan error, 2)
	var scaleStatus error

	go runScale(nodeId, scaleStatusChan)

	// wait for new requests and on each received run SCALE-MAMBA
	for {
		// receive request
		docsnap, err := iter.Next()

		if err != nil {
			fmt.Println("bad request", err)
			continue
		}
		fmt.Println("Node " + strconv.Itoa(nodeId) + ": request recieved")

		// read request
		var req consumer.Request
		err = docsnap.DataTo(&req)
		if err != nil {
			fmt.Println("bad request", err)
			continue
		}

		// download the data shares needed for the computation
		dataLen, err := downloadNodeShares(nodeId, &req, client)
		if err != nil {
			fmt.Println("bad request", err)
			sendFailMPC(err, nodeId, client, req)
			continue
		}


		// check if SCALE ok
		scaleStatus = <-scaleStatusChan
		if scaleStatus != nil {
			go runScale(nodeId, scaleStatusChan)
			scaleStatus = <-scaleStatusChan
			if scaleStatus != nil {
				fmt.Println("computation failed", scaleStatus)
				sendFailMPC(scaleStatus, nodeId, client, req)
				fmt.Println("node crash")
				errChan <- scaleStatus
				return
			}
		}
		scaleStatusChan <- nil

		// execute the computation of the node
		err = nodeTrigger(nodeId, dataLen, req.FuncName)
		if err != nil {
			fmt.Println("computation failed", err)
			sendFailMPC(err, nodeId, client, req)
			continue
		}

		// read the result that was obtained by computation;
		// for now this is done by Node 0 providing the result
		var res *big.Int
		if nodeId == 0 {
			res, err = loadResult()
			if err != nil {
				fmt.Println("result unreadable", err)
				sendFailMPC(err, nodeId, client, req)
				continue
			}
		} else {
			res = big.NewInt(0)
		}

		// send the result to the database
		_, err = client.Collection("Results").Doc(req.Name).Update(ctx, []firestore.Update{
			{Path: "share" + strconv.Itoa(nodeId+1), Value: res.String()},
			{Path: "error" + strconv.Itoa(nodeId+1), Value: ""}})
		if err != nil {
			fmt.Println("bad request", err)
			continue
		}
		if nodeId == 0 {
			fmt.Println("Node " + strconv.Itoa(nodeId) + ": result provided")
		}
	}

	return
}

func sendFailMPC(err error, nodeId int, client *firestore.Client, req consumer.Request) {
	// send the fail to the database
	ctx := context.Background()
	_, newErr := client.Collection("Results").Doc(req.Name).Update(ctx, []firestore.Update{
		{Path: "error" + strconv.Itoa(nodeId+1), Value: err.Error()}})
	if newErr != nil {
		fmt.Println("failed sending an error")
	}
}

// loadResult is a helping function that loads the result obtained by
// MPC computation.
func loadResult() (*big.Int, error) {
	for {
		_, err := ioutil.ReadFile(scaleMambaPath + "Input/finish.txt")
		if err == nil {
			cmd := exec.Command("rm", "Input/finish.txt")
			cmd.Dir = scaleMambaPath
			err = cmd.Run()
			if err !=nil {
				continue
			}
			break
		}

		time.Sleep(100 * time.Millisecond)
	}
	f, err := ioutil.ReadFile(scaleMambaPath + "Input/result.txt")
	if err !=nil {
		return nil, err
	}

	str := string(f)
	str = str[0 : len(str)-1]
	res, _ := new(big.Int).SetString(str, 10)

	return res, nil
}

// downloadNodeShares is a helping function that loads the data shares provided to
// the node for the MPC computation.
func downloadNodeShares(nodeId int, request *consumer.Request, client *firestore.Client) (int, error) {
	docsnap, err := client.Collection("EncryptedData").Doc("MPC").Collection(request.UserName).Doc(request.VecName).Collection("nodes").Doc(strconv.Itoa(nodeId)).Get(context.Background())
	if err != nil {
		return 0, err
	}

	var dataShare provider.DataShareMPC
	err = docsnap.DataTo(&dataShare)
	if err != nil {
		return 0, err
	}

	// extract share
	var dataShareVec data.Vector
	bufCipher := new(bytes.Buffer)
	bufCipher.Write(dataShare.Share)
	decoderCipher := gob.NewDecoder(bufCipher)
	err = decoderCipher.Decode(&dataShareVec)
	if err != nil {
		return 0, err
	}

	f, err := os.Create( scaleMambaPath + "Input/data" + strconv.Itoa(nodeId) + ".txt")
	for j := 0; j < len(dataShareVec); j++ {
		_, err = f.WriteString(dataShareVec[j].String() + "\n")
		if err != nil {
			return 0, err
		}
	}

	return len(dataShareVec), nil
}

func runScale(nodeId int, errChan chan error) {
	// prepare settings for SCALE

	// remove previous compiled program if there
	cmd := exec.Command("rm", "Programs/prototype/node"+strconv.Itoa(nodeId)+
		"/node"+strconv.Itoa(nodeId)+".mpc")
	cmd.Dir = scaleMambaPath
	cmd.Run()

	// prepare dummy MAMBA program for SCALE
	cmd = exec.Command("cp", "Programs/prototype/functions/restart.mpc", "Programs/prototype/node"+
		strconv.Itoa(nodeId)+"/node"+strconv.Itoa(nodeId)+".mpc")
	cmd.Dir = scaleMambaPath
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
		errChan <- err
		return
	}

	// compile the dummy MAMBA program
	cmd = exec.Command("./compile.sh", "Programs/prototype/node"+strconv.Itoa(nodeId))
	cmd.Dir = scaleMambaPath
	err = cmd.Run()
	if err != nil {
		errChan <- err
		return
	}
	errChan <- nil
	// start SCALE node that will prepare itself for future computation
	cmd = exec.Command("./Player.x", strconv.Itoa(nodeId), "Programs/prototype/node"+strconv.Itoa(nodeId))
	cmd.Dir = scaleMambaPath
	err = cmd.Run()
	if err != nil {
		errChan <- err
	} else {
		errChan <- fmt.Errorf("node finished")
	}
}

// nodeTrigger is a helping function that executes the MPC computation done by one
// node.
func nodeTrigger(nodeId int, dataLen int, funcName string) error {
	var err error

	// remove previous finished note if there
	if nodeId == 0 {
		cmd := exec.Command("rm", "Input/finish.txt")
		cmd.Dir = scaleMambaPath
		cmd.Run()
	}

	// remove previous compiled program if there
	cmd := exec.Command("rm", "Programs/prototype/node"+strconv.Itoa(nodeId)+
		"/node"+strconv.Itoa(nodeId)+".mpc")
	cmd.Dir = scaleMambaPath
	cmd.Run()

	// write a MAMBA program by replacing all "LEN" words in a template to the actual length of vectors
	cmd = exec.Command("/bin/sh", "-c", "sed 's/LEN/"+strconv.Itoa(dataLen)+
		"/g' Programs/prototype/functions/"+funcName+".mpc >> Programs/prototype/node"+
		strconv.Itoa(nodeId)+ "/node"+strconv.Itoa(nodeId)+".mpc")
	cmd.Dir = scaleMambaPath
	err = cmd.Run()
	if err != nil {
		return err
	}

	// compile the the MAMBA program
	cmd = exec.Command("./compile.sh", "Programs/prototype/node"+strconv.Itoa(nodeId))
	cmd.Dir = scaleMambaPath
	err = cmd.Run()
	if err != nil {
		return err
	}

	// trigger SCALE computation node to compute the result
	f, err := os.Create(scaleMambaPath+"Input/trigger" + strconv.Itoa(nodeId) + ".txt")
	if err != nil {
		return err
	}
	_, err = f.Write([]byte("restart\n"))
	if err != nil {
		return err
	}
	f.Close()

	return nil
}
