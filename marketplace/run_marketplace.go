package marketplace

import "fmt"

func RunMarketPlace() {
	errChan := make(chan error)
	for i := 0; i < 3; i++ {
		go NodeMPC(i, errChan)
	}
	go MarketPlaceMPC(errChan)
	go MarketPlaceFE(errChan)
	err := <- errChan
	fmt.Println(err)
}
