package main

import (
	"os"
	"fmt"
	"bitbucket.org/tilenmarc/kraken_proto/data_provider"
	"bitbucket.org/tilenmarc/kraken_proto/data_consumer"
	"bitbucket.org/tilenmarc/kraken_proto/marketplace"
)

func main() {
	args := os.Args[1:]
	fmt.Println(args)

	if args[0] == "marketplace" {
		marketplace.RunMarketPlace()
	} else
	if args[0] == "provider" {
		provider.RunProvider()
	} else
	if args[0] == "consumer" {
		consumer.RunConsumer()
	}
}
