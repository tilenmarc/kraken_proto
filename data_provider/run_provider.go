package provider

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"strings"

	"github.com/fentec-project/gofe/data"
)

func fileToVec(fileName string) (data.Vector, error) {
	f, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	strF := string(f)
	if strF[len(strF)-1] == '\n' {
		strF = strF[:len(strF)-1]
	}

	numbers := strings.Split(strF, " ")
	v := make(data.Vector, len(numbers))
	for i, n := range numbers {
		v[i], _ = new(big.Int).SetString(n, 10)
	}

	return v, err
}

func RunProvider() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Type FE or MPC for the choice of platform\n")
	platform, _ := reader.ReadString('\n')
	platform = platform[:len(platform)-1]

	fmt.Print("Enter a name of data provider (for example type TUG): ")
	name, _ := reader.ReadString('\n')
	name = name[:len(name)-1]

	fmt.Print("Enter name of data (for example type grades): ")
	nameVec, _ := reader.ReadString('\n')
	nameVec = nameVec[:len(nameVec)-1]

	fmt.Print("Enter a file with data, i.e. a vector of not too big positive integers. " +
		"This should be a text file with integers written in one line separated by whitespaces. " +
		"See data/example_data.txt for an example (you can type data/example_data.txt):")
	nameFile, _ := reader.ReadString('\n')
	nameFile = nameFile[:len(nameFile)-1]

	dataVec, err := fileToVec(nameFile)
	if err != nil {
		log.Fatalln(err)
	}
	//fmt.Println(dataVec)

	l := len(dataVec)

	if platform == "FE" {
		p, err := NewDataProviderFE(name, l)
		if err != nil {
			log.Fatalln(err)
		}

		// data provider sends encrypted vector
		err = p.SendEncrypted(dataVec, nameVec)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println("sent " + nameVec)

		// data provider sends key for decrypting average
		averageVec := data.NewConstantVector(l, big.NewInt(1))
		err = p.SendDerivedKey(averageVec, "sum")
		if err != nil {
			log.Fatalln(err)
		}
	} else if platform == "MPC" {
		p, err := NewDataProviderMPC(name)
		if err != nil {
			log.Fatalln(err)
		}

		// data provider sends encrypted vector
		err = p.SendSharesMPC(dataVec, nameVec)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println("sent " + nameVec)


	}

}
