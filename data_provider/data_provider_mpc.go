package provider

import (
	"github.com/fentec-project/gofe/data"
	//"google.golang.org/api/option"
	"bytes"
	"context"
	"encoding/gob"
	"math/big"

	"strconv"

	"firebase.google.com/go"
	"github.com/fentec-project/gofe/sample"
	"google.golang.org/api/option"
)

type DataProviderMPC struct {
	Id string
}

type DataShareMPC struct {
	Name  string `firestore:"name"`
	Share []byte `firestore:"share"`
	Sign  string `firestore:"signature"`
}

// NewDataProviderMPC creates a new instance of a data provider that will
// be generating data for MPC computation.
func NewDataProviderMPC(name string) (*DataProviderMPC, error) {
	if name == "" {
		sampler := sample.NewUniform(new(big.Int).Exp(big.NewInt(2), big.NewInt(128), nil))
		id, err := sampler.Sample()

		if err != nil {
			return nil, err
		}
		name = id.String()
	}

	return &DataProviderMPC{Id: name}, nil
}

// createShares is a helping function that splits a vector
// input into 3 random parts x_1, x_2, x_3 such that
// x_1 + x_2 + x_3 = input (mod 2^32).
func createShares(input data.Vector) (data.Matrix, error) {
	twoPow32 := new(big.Int).Exp(big.NewInt(2), big.NewInt(32), nil)
	sampler := sample.NewUniform(twoPow32)
	res, err := data.NewRandomMatrix(3, len(input), sampler)
	if err != nil {
		return nil, err
	}
	res[2] = input.Sub(res[0]).Sub(res[1]).Mod(twoPow32)

	return res, nil
}

// SendSharesMPC splits the input vector into 3 shares and uploads them
// to the database for the computation nodes to use them.
func (d DataProviderMPC) SendSharesMPC(input data.Vector, name string) error {
	sharesMat, err := createShares(input)
	if err != nil {
		return err
	}

	shares := make([]DataShareMPC, 3)
	for i := 0; i < 3; i++ {
		shares[i].Name = name + strconv.Itoa(i)

		bufShare := new(bytes.Buffer)
		encoderShare := gob.NewEncoder(bufShare)
		err = encoderShare.Encode(&sharesMat[i])
		if err != nil {
			return err
		}
		shares[i].Share = bufShare.Bytes()

		shares[i].Sign = "bla"
	}

	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return err
	}
	client, err := app.Firestore(ctx)
	if err != nil {
		return err
	}

	for i := 0; i < 3; i++ {
		_, err = client.Collection("EncryptedData").Doc("MPC").Collection(d.Id).Doc(name).Collection("nodes").Doc(strconv.Itoa(i)).Set(ctx, shares[i])
		if err != nil {
			return err
		}
	}

	_, err = client.Collection("EncryptedData").Doc("MPC").Collection(d.Id).Doc(name).Set(ctx, map[string]string{"name":name})

	return err
}
