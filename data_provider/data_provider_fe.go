package provider

import (
	"github.com/fentec-project/gofe/data"
	//"google.golang.org/api/option"
	"bytes"
	"context"
	"encoding/gob"
	"math/big"

	"firebase.google.com/go"
	"github.com/fentec-project/gofe/innerprod/fullysec"
	"github.com/fentec-project/gofe/sample"
	"google.golang.org/api/option"
)

type DataProviderFE struct {
	Id     string
	Scheme *fullysec.Damgard
	SecKey *fullysec.DamgardSecKey
	PubKey data.Vector
}

type ProviderCipherFE struct {
	Name   string `firestore:"name"`
	Cipher []byte `firestore:"cipher"`
	Sign   string `firestore:"signature"`
}

type ProviderDerivedKeyFE struct {
	Name     string `firestore:"name"`
	InnerVec []byte `firestore:"vec"`
	FEKey    []byte `firestore:"key"`
	Sign     string `firestore:"signature"`
}
// NewDataProviderFE creates a new instance of the data provider, that will
// be providing vectors of length l.
func NewDataProviderFE(name string, l int) (*DataProviderFE, error) {
	if name == "" {
		sampler := sample.NewUniform(new(big.Int).Exp(big.NewInt(2), big.NewInt(128), nil))
		id, err := sampler.Sample()

		if err != nil {
			return nil, err
		}
		name = id.String()
	}

	scheme, err := fullysec.NewDamgardPrecomp(l, 2048, big.NewInt(1000))
	if err != nil {
		return nil, err
	}
	secKey, pubKey, err := scheme.GenerateMasterKeys()
	if err != nil {
		return nil, err
	}

	return &DataProviderFE{Id: name, Scheme: scheme, SecKey: secKey, PubKey: pubKey}, nil
}

// SendEncrypted uploads to the database a vector encrypted with FE.
func (d DataProviderFE) SendEncrypted(input data.Vector, name string) error {
	cipher, err := d.Scheme.Encrypt(input, d.PubKey)
	if err != nil {
		return err
	}

	bufCipher := new(bytes.Buffer)
	encoderCipher := gob.NewEncoder(bufCipher)

	err = encoderCipher.Encode(cipher)
	if err != nil {
		return err
	}

	writeData := ProviderCipherFE{Name: name, Cipher: bufCipher.Bytes(), Sign: "bla"}

	// TODO: manage access to database
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return err
	}
	_, err = client.Collection("EncryptedData").Doc("FE").Collection(d.Id).Doc(name).Set(context.Background(), writeData)

	return err
}

// SendEncrypted uploads to the database a FE key corresponding to an inner product vector y.
func (d DataProviderFE) SendDerivedKey(y data.Vector, name string) error {
	feKey, err := d.Scheme.DeriveKey(d.SecKey, y)
	if err != nil {
		return err
	}

	// encode feKey and y in binary
	bufKey := new(bytes.Buffer)
	encoderKey := gob.NewEncoder(bufKey)
	err = encoderKey.Encode(*feKey)
	if err != nil {
		return err
	}
	bufY := new(bytes.Buffer)
	encoderY := gob.NewEncoder(bufY)
	err = encoderY.Encode(y)
	if err != nil {
		return err
	}
	writeData := ProviderDerivedKeyFE{Name: name, InnerVec: bufY.Bytes(), FEKey: bufKey.Bytes(), Sign: "bla"}

	// TODO: manage access to database
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return err
	}
	_, err = client.Collection("Functions").Doc("FE").Collection(d.Id).Doc(name).Set(context.Background(), writeData)

	return err
}
