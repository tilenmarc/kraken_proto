package main_test

import (
	"fmt"
	"log"
	"math/big"
	"strconv"

	"bitbucket.org/tilenmarc/kraken_proto/data_consumer"
	"bitbucket.org/tilenmarc/kraken_proto/data_provider"
	"github.com/fentec-project/gofe/data"
	"github.com/fentec-project/gofe/sample"
	"testing"
)

func TestMPC(t *testing.T) {
	// data provider will provide vectors of length l
	l := 1000
	p, err := provider.NewDataProviderMPC("XLAB")
	if err != nil {
		log.Fatalln(err)
	}

	// data provider will provide numVec (random) vectors
	numVec := 50
	sampler := sample.NewUniform(big.NewInt(1024))

	for i := 0; i < numVec; i++ {
		vec, err := data.NewRandomVector(l, sampler)
		if err != nil {
			log.Fatalln(err)
		}
		// data provider sends encrypted vector
		err = p.SendSharesMPC(vec, "vector"+strconv.Itoa(i))
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println("sent vector" + strconv.Itoa(i))
	}

	// data consumer downloads the average
	c := consumer.NewDataConsumer()
	sum, err := c.RequestDataMPC(p.Id, "vector1", "sum")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(sum)
}



func TestFE(t *testing.T) {
	fmt.Println()
	// data provider will provide vectors of length l
	l := 50
	providerName := "XLAB"
	p, err := provider.NewDataProviderFE(providerName, l)
	if err != nil {
		log.Fatalln(err)
	}

	// data provider will provide numVec (random) vectors
	numVec := 20
	sampler := sample.NewUniform(big.NewInt(10))

	for i := 0; i < numVec; i++ {
		vec, err := data.NewRandomVector(l, sampler)
		if err != nil {
			log.Fatalln(err)
		}
		// data provider sends encrypted vector
		err = p.SendEncrypted(vec, "vector"+strconv.Itoa(i))
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println("sent vector" + strconv.Itoa(i))
	}

	// data provider sends key for decrypting average
	sumVec := data.NewConstantVector(l, big.NewInt(1))
	err = p.SendDerivedKey(sumVec, "sum")
	if err != nil {
		log.Fatalln(err)
	}

	// data consumer downloads the average
	c := consumer.NewDataConsumer()
	sum, err := c.RequestData(p.Id, "vector1", "sum", "FE")
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(sum)
}
