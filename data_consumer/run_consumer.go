package consumer

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func RunConsumer() {
	// create new data consumer
	c := NewDataConsumer()
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Type FE or MPC for the choice of platform\n")
	platform, _ := reader.ReadString('\n')
	platform = platform[:len(platform)-1]


	prov, err := c.RequestInfoProviders(platform)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Print("Enter name of data provider, options are\n")
	fmt.Println(prov)
	name, _ := reader.ReadString('\n')
	name = name[:len(name)-1]

	dataNames, err := c.RequestInfoProvidersData(name, platform)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Print("Enter name of data, options are\n")
	fmt.Println(dataNames)

	nameVec, _ := reader.ReadString('\n')
	nameVec = nameVec[:len(nameVec)-1]

	funcNames, err := c.RequestInfoProvidersFunctions(name, platform)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Print("Enter function, options are\n")
	fmt.Println(funcNames)
	nameFunc, _ := reader.ReadString('\n')
	nameFunc = nameFunc[:len(nameFunc)-1]

	// data consumer requests for the average
	avg, err := c.RequestData(name, nameVec, nameFunc, platform)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("recieved result is", avg)
}
