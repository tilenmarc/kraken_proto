package consumer

import (
	"context"
	"fmt"
	"math/big"

	"bitbucket.org/tilenmarc/kraken_proto/data_provider"
	"firebase.google.com/go"
	"github.com/fentec-project/gofe/sample"
	"google.golang.org/api/option"
	"cloud.google.com/go/firestore"
)

type DataConsumer struct {
}

func NewDataConsumer() *DataConsumer {
	return &DataConsumer{}
}

type Request struct {
	Name     string
	UserName string
	VecName  string
	FuncName string
}

type Result struct {
	Res string `firestore:"result"`
	Error string `firestore:"error"`
}

type ResultShares struct {
	Share1 string `firestore:"share1"`
	Share2 string `firestore:"share2"`
	Share3 string `firestore:"share3"`
	Error1 string `firestore:"error1"`
	Error2 string `firestore:"error2"`
	Error3 string `firestore:"error3"`
}

// RequestData asks for data computation using MPC or FE upon request.
func (d DataConsumer) RequestData(userName, vecName, funcName, platform string) (*big.Int, error) {
	var res *big.Int
	var err error
	if platform == "MPC" {
		res, err = d.RequestDataMPC(userName, vecName, funcName)
	} else if platform == "FE" {
		res, err = d.RequestDataFE(userName, vecName, funcName)
	} else {
		return nil, fmt.Errorf("platform should be FE or MPC")
	}

	return res, err
}

// RequestDataMPC asks the database for data that can be obtained using MPC computation. It
// creates the
func (d DataConsumer) RequestDataMPC(userName, vecName, funcName string) (*big.Int, error) {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return nil, err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}

	sampler := sample.NewUniform(new(big.Int).Exp(big.NewInt(2), big.NewInt(128), nil))
	randNum, err := sampler.Sample()
	requestId := randNum.String()

	_, err = client.Collection("Results").Doc(requestId).Set(ctx, ResultShares{})
	if err != nil {
		return nil, err
	}

	iter := client.Collection("Results").Doc(requestId).Snapshots(ctx)
	if err != nil {
		return nil, err
	}
	defer iter.Stop()

	_, err = iter.Next()
	if err != nil {
		return nil, err
	}

	_, err = client.Collection("DataRequests").Doc("MPC").Set(ctx,
		Request{Name: requestId, UserName: userName, VecName: vecName, FuncName: funcName})

	fmt.Println("request given, waiting for response")

	var shares ResultShares
	for i := 0; i < 3; i++ {
		docsnap, err := iter.Next()
		if err != nil {
			return nil, err
		}

		if i < 2 {
			continue
		}

		err = docsnap.DataTo(&shares)
		if err != nil {
			return nil, err
		}
		if shares.Error1 != "" {
			_, err = client.Collection("Results").Doc(requestId).Delete(ctx)
			if err != nil {
				return nil, err
			}
			return nil, fmt.Errorf(shares.Error1)
		}
		if shares.Error2 != "" {
			_, err = client.Collection("Results").Doc(requestId).Delete(ctx)
			if err != nil {
				return nil, err
			}
			return nil, fmt.Errorf(shares.Error2)
		}
		if shares.Error3 != "" {
			_, err = client.Collection("Results").Doc(requestId).Delete(ctx)
			if err != nil {
				return nil, err
			}
			return nil, fmt.Errorf(shares.Error3)
		}
		break
	}
	sharesNum := make([]*big.Int, 3)
	sharesNum[0], _ = new(big.Int).SetString(shares.Share1, 10)
	sharesNum[1], _ = new(big.Int).SetString(shares.Share2, 10)
	sharesNum[2], _ = new(big.Int).SetString(shares.Share3, 10)

	res := big.NewInt(0)
	for i := 0; i < 3; i++ {
		res.Add(res, sharesNum[i])
	}
	twoPow64 := new(big.Int).Exp(big.NewInt(2), big.NewInt(64), nil)
	res.Mod(res, twoPow64)

	fmt.Println("result provided")

	_, err = client.Collection("Results").Doc(requestId).Delete(ctx)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (d DataConsumer) RequestDataFE(userName, vecName, funcName string) (*big.Int, error) {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return nil, err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}

	sampler := sample.NewUniform(new(big.Int).Exp(big.NewInt(2), big.NewInt(128), nil))
	randNum, err := sampler.Sample()
	requestId := randNum.String()

	_, err = client.Collection("Results").Doc(requestId).Set(ctx, Result{})
	if err != nil {
		return nil, err
	}

	iter := client.Collection("Results").Doc(requestId).Snapshots(ctx)
	if err != nil {
		return nil, err
	}
	defer iter.Stop()

	_, err = iter.Next()
	if err != nil {
		return nil, err
	}

	_, err = client.Collection("DataRequests").Doc("FE").Set(ctx,
		Request{Name: requestId, UserName: userName, VecName: vecName, FuncName: funcName})

	fmt.Println("request given, waiting for response")

	var resNum *big.Int
	for {
		docsnap, err := iter.Next()
		if err != nil {
			return nil, err
		}

		var res Result
		docsnap.DataTo(&res)
		if res.Error != "" {
			_, err = client.Collection("Results").Doc(requestId).Delete(ctx)
			if err != nil {
				return nil, err
			}
				return nil, fmt.Errorf(res.Error)
		}

		resNum, _ = new(big.Int).SetString(res.Res, 10)
		break
	}
	fmt.Println("result provided")

	_, err = client.Collection("Results").Doc(requestId).Delete(ctx)
	if err != nil {
		return nil, err
	}

	return resNum, nil
}



func (d DataConsumer) RequestInfoProviders(platform string) ([]string, error) {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return nil, err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}
	providersColls, err := client.Collection("EncryptedData").Doc(platform).Collections(ctx).GetAll()
	providers := make([]string, len(providersColls))
	for i, e := range providersColls {
		providers[i] = e.ID
	}

	return providers, nil
}

func (d DataConsumer) RequestInfoProvidersData(providerName, platform string) ([]string, error) {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return nil, err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}
	providersDocs, err := client.Collection("EncryptedData").Doc(platform).Collection(providerName).Documents(ctx).GetAll()
	if err != nil {
		return nil, err
	}
	dataNames := make([]string, len(providersDocs))
	for i, e := range providersDocs {
		var enc provider.ProviderCipherFE
		err = e.DataTo(&enc)
		if err != nil {
			return nil, err
		}
		dataNames[i] = enc.Name
	}

	return dataNames, nil
}

func (d DataConsumer) RequestInfoProvidersFunctions(providerName, platform string) ([]string, error) {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return nil, err
	}
	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}
	var providersDocs []*firestore.DocumentSnapshot
	if platform == "FE" {
		providersDocs, err = client.Collection("Functions").Doc(platform).Collection(providerName).Documents(ctx).GetAll()
		if err != nil {
			return nil, err
		}
	} else if platform == "MPC" {
		providersDocs, err = client.Collection("Functions").Doc(platform).Collection("All").Documents(ctx).GetAll()
	}
	functs := make([]string, len(providersDocs))
	for i, e := range providersDocs {
		var enc provider.ProviderCipherFE
		err = e.DataTo(&enc)
		if err != nil {
			return nil, err
		}
		functs[i] = enc.Name
	}

	return functs, nil
}
//
//func (d DataConsumer) GetData(userName, vecName, funcName string) (*big.Int, error) {
//	// get encryption from database
//	opt := option.WithCredentialsFile("./kraken-prototype-firebase-adminsdk-mi2ta-df09965224.json")
//	app, err := firebase.NewApp(context.Background(), nil, opt)
//	if err != nil {
//		return nil, err
//	}
//	client, err := app.Firestore(context.Background())
//	if err != nil {
//		return nil, err
//	}
//
//	docsnap, err := client.Collection("EncryptedData").Doc("FE").Collection(userName).Doc(vecName).Get(context.Background())
//	if err != nil {
//		return nil, err
//	}
//
//	var cipherData provider.ProviderCipherFE
//	err = docsnap.DataTo(&cipherData)
//	if err != nil {
//		return nil, err
//	}
//
//	// extract cipher
//	var cipher data.Vector
//	bufCipher := new(bytes.Buffer)
//	bufCipher.Write(cipherData.Cipher)
//
//	decoderCipher := gob.NewDecoder(bufCipher)
//	err = decoderCipher.Decode(&cipher)
//	if err != nil {
//		return nil, err
//	}
//
//	docsnap, err = client.Collection("DerivedKeys").Doc("FE_keys").Collection(userName).Doc(funcName).Get(context.Background())
//	if err != nil {
//		return nil, err
//	}
//
//	var keyData provider.ProviderDerivedKeyFE
//	err = docsnap.DataTo(&keyData)
//	if err != nil {
//		return nil, err
//	}
//
//	// extract FEkey
//	var feKey fullysec.DamgardDerivedKey
//	bufKey := new(bytes.Buffer)
//	bufKey.Write(keyData.FEKey)
//
//	decoderKey := gob.NewDecoder(bufKey)
//	err = decoderKey.Decode(&feKey)
//	if err != nil {
//		return nil, err
//	}
//	// extract inner product vector
//	var y data.Vector
//	bufY := new(bytes.Buffer)
//	bufY.Write(keyData.InnerVec)
//
//	decoderInner := gob.NewDecoder(bufY)
//	err = decoderInner.Decode(&y)
//	if err != nil {
//		return nil, err
//	}
//
//	bound := big.NewInt(10)
//	scheme, err := fullysec.NewDamgardPrecomp(len(cipher)-2, 2048, bound)
//
//	res, err := scheme.Decrypt(cipher, &feKey, y)
//
//	return res, err
//}
