FROM ubuntu

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y \
  git \
  yasm \
  python \
  gcc \
  g++ \
  cmake \
  make \
  curl \
  wget \
  apt-transport-https \
  m4 \
  zip \
  unzip \
  vim \
  build-essential

# Get SCALE-MAMBA
RUN git clone https://github.com/tilenmarc/SCALE-MAMBA.git
WORKDIR /SCALE-MAMBA

# Install dependencies of SCALE-MAMBA
RUN mkdir -p /local
WORKDIR /local

# install MPIR
RUN curl -O 'http://mpir.org/mpir-3.0.0.tar.bz2'
RUN tar xf mpir-3.0.0.tar.bz2
WORKDIR /local/mpir-3.0.0
RUN ./configure --enable-cxx --prefix="/local/mpir"
RUN make && make check && make install

# install OpenSSL 1.1.0
WORKDIR /local
RUN curl -O https://www.openssl.org/source/old/1.1.1/openssl-1.1.1e.tar.gz
RUN tar -xf openssl-1.1.1e.tar.gz
WORKDIR /local/openssl-1.1.1e
RUN ./config --prefix="/local/openssl"
RUN make && make install

# install crypto++
WORKDIR /local
RUN curl -O https://www.cryptopp.com/cryptopp800.zip
RUN unzip cryptopp800.zip -d cryptopp800
WORKDIR /local/cryptopp800
RUN make && make install PREFIX="/local/cryptopp"

ENV PATH="/local/openssl/bin/:${PATH}"
ENV C_INCLUDE_PATH="/local/openssl/include/:${C_INCLUDE_PATH}"
ENV CPLUS_INCLUDE_PATH="/local/openssl/include/:${CPLUS_INCLUDE_PATH}"
ENV LIBRARY_PATH="/local/openssl/lib/:${LIBRARY_PATH}"
ENV LD_LIBRARY_PATH="/local/openssl/lib/:${LD_LIBRARY_PATH}"
ENV C_INCLUDE_PATH="/local/mpir/include/:${C_INCLUDE_PATH}"
ENV CPLUS_INCLUDE_PATH="/local/mpir/include/:${CPLUS_INCLUDE_PATH}"
ENV LIBRARY_PATH="/local/mpir/lib/:${LIBRARY_PATH}"
ENV LD_LIBRARY_PATH="/local/mpir/lib/:${LD_LIBRARY_PATH}"
ENV CPLUS_INCLUDE_PATH="/local/cryptopp/include/:${CPLUS_INCLUDE_PATH}"
ENV LIBRARY_PATH="/local/cryptopp/lib/:${LIBRARY_PATH}"
ENV LD_LIBRARY_PATH="/local/cryptopp/lib/:${LD_LIBRARY_PATH}"

## Configure and compile SCALE-MAMBA
WORKDIR /SCALE-MAMBA
RUN cp CONFIG CONFIG.mine
RUN echo 'ROOT = /SCALE-MAMBA' >> CONFIG.mine
RUN echo 'OSSL = /local/openssl' >> CONFIG.mine

RUN cp Auto-Test-Data/Cert-Store/* Cert-Store/
RUN cp Auto-Test-Data/1/* Data/

RUN make progs

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN ./compile.sh Programs/tutorial/

WORKDIR /

## Install golang
RUN wget --no-check-certificate https://dl.google.com/go/go1.14.1.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.14.1.linux-amd64.tar.gz
ENV PATH="/usr/local/go/bin:${PATH}"

## Install kraken prototype and dependencies
RUN go get -u -t bitbucket.org/tilenmarc/kraken_proto/...

# Compile it
WORKDIR /root/go/src/bitbucket.org/tilenmarc/kraken_proto
RUN go build .

