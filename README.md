# Kraken data processing unit prototype #


## Install ##
Use `go get -u -t bitbucket.org/tilenmarc/kraken_proto/...` to install the prototype with all the dependencies.
This should install the prototype in `$GOPATH/src/bitbucket.org/tilenmarc/kraken_proto`.

Additionally install slightly modified SCALE-MAMBA from `https://github.com/tilenmarc/SCALE-MAMBA`. That is,
the installation should be the same as in the case of `https://github.com/KULeuven-COSIC/SCALE-MAMBA`, see
`https://homes.esat.kuleuven.be/~nsmart/SCALE/Documentation.pdf`.

Finally, change in file `$GOPATH/src/bitbucket.org/tilenmarc/kraken_proto/` the variable `scaleMambaPath` to
 the destination where you put SCALE-MAMBA. For example the line should look like:
 
 `var scaleMambaPath = "/my/path/to/SCALE-MAMBA/"` 
 
#### Docker ####
Instead of the above you can use Docker. Run

`docker build --tag kraken_proto .`

This will install the prototype and SCALE-MAMBA. It takes quite some time to install all the dependencies
and compiles it. By running

`docker run -it kraken_proto`

you should have an interactive terminal with executable file `kraken_proto` in
`/root/go/src/bitbucket.org/tilenmarc/kraken_proto` directory.


## Running example
The implementation consists of three entities: data producer, marketplace and data consumer.
Navigate to `$GOPATH/src/bitbucket.org/tilenmarc/kraken_proto` (in the case it was build
with docker this is `/root/go/src/bitbucket.org/tilenmarc/kraken_proto`). Run the marketplace with
command:

`./kraken_proto marketplace`

This can be computationally difficult since it starts MPC and FE marketplace
and 3 MPC nodes (it starts 3 players in SCALE-MAMBA what should take approx 
3 x 1.5GB of RAM).

In a different terminal run

`./kraken_proto consumer`

to start a data consumer (some data is already in the database to play with).

To start a data provider type 

`./kraken_proto provider` 

All the above must be run from `$GOPATH/src/bitbucket.org/tilenmarc/kraken_proto`
directory since it includes keys to connect to the server.



## Structure ##
The implementation consists of three entities: data producer, marketplace and data consumer.
It allows the data producer to encrypt its data, so that the marketplace can
calculate a desired function on the data and offer the result to the data consumer. Calculation can be based on
MPC or FE.

### Database ###
Communication is done through a database Firebase which replaces the blockchain. We picked it because it is free
if the traffic is not to big, so it is good for testing. It has a simple API for various languages.

### Data Producer ###
Data producer sends to the database encrypted data vectors. Here a choice has to be made if 
calculation will be done with FE or MPC: 
- In the FE case, the encryption of data has to be done with FE. Moreover, the
data provider sends to the marketplace FE keys allowing to compute chosen
functions on the data. Encrytpion and Key Derivation is done using GoFE.
- In the MPC case, the data `x` is split into 3 parts `x_1, x_2, x_3` such that 
`x_1 + x_2 + x_3 = x`, and each part is sent separately to be used
by a MPC computation node 

### Data Consumer ###
Data consumer simply sends a request to the database asking for some
function to be computed on chosen data.

### MarketPlace ###
Marketplace waits for a request from a data consumer. When received it does
the following based on which protocol is intended:
- In FE case it uses FE key to calculate the result of the function with GoFE.
 Then it sends the result back to consumer.
- In MPC case we have also MPC nodes waiting to be triggered. Upon request by
data consumer the marketplace sends requests for computation to 3 nodes.
Each node does the following. First it downloads the part of the data that
data provider sent for it. Then based on the function asked to be computed
it compiles an appropriate MAMBA program. Finally it *restarts* SCALE where
it provides its part of the data and calculates with other nodes the result.
The *restart* is an option of SCALE that uses pre-computed triples from
offline phase so that online phase is faster.
The result is exposed to node 0 which sends it to the consumer.


## Problems, TODOs, open questions ##

- For now everyone can access the database and do whatever, but it makes no sense
to take care of this since it is only a prototype
- TODO: Data transmitted between consumer and marketplace, or producer and marketplace
is for now not encrypted.
- TODO: there is no signing implemented, key distribution etc
- Managing requests through database is problematic, I don't know what happens if
to request come up simultaneously or a lot of requests appear. Maybe we shall need
something like Apache Kafka when we go beyond a prototype
- SCALE-MAMBA and marketplace in Golang are communicating by writing to files. This is
probably not ok, but was simple to implement.
- Not sure how stable everything is, does SCALE crashes if run too long, etc. For now we
have a very basic (if any) error management.
- multiple source data, streaming data,...
